function bubbleSort(arr) {
  /*  function takes an unordered
      array and returns the sorted
      array in ascending order
  */
  var sorted = false;
  while (sorted == false) {
    var swaps = 0;
    for (var i=0;i<arr.length-1;i++) {
      var a = arr[i];
      var b = arr[i+1];
      if (a > b) {
        arr[i] = b;
        arr[i+1] = a;
        swaps += 1;
      }
    }
    if (swaps == 0) {
      sorted = true;
    }
  }
  return arr;
}

function mergeSort(arr) {
  /*  function takes an unordered
      array and returns the sorted
      array in ascending order
  */
  // split the array into N sub-arrays containing 1 element
  var parts = splitArr(arr);
  // merge the parts back together
  var merged = merge(parts);
}

function merge(arr) {
  /*  merges an array of N sub-arrays
      back into a single ordered array
      (called by mergeSort)
  */
  newArr = [];
  while (arr.length > 0) {
    if (arr.length == 1) {
      newArr.push(arr[0]);
      arr.splice(0, 1);
    }
    else {
      newArr.push(mergeParts(arr[0], arr[1]));
      arr.splice(0, 2);
    }
  }
  if (newArr.length > 1) {
    newArr = merge(newArr);
  }
  return newArr;
}

function mergeParts(arr1, arr2) {
  /*  merges 2 sub-arrays into a single
      ordered array
      (called by mergeSort > merge)
  */
  for (var i=arr2.length-1;i>=0;i--) {
    var j = arr1.length - 1;
    while (arr1[j] > arr2[i]) {
      j--;
    }
    arr1.splice(j+1,0,arr2[i]);
    arr2.pop();
  }
  return arr1;
}

function splitArr(arr, result=[]) {
  /*  function performs the splitting of
      an N element array into N sub-arrays
      (called by mergeSort)
  */
  var middle = Math.ceil(arr.length/2);
  var leftBit = arr.slice(0,middle);
  var rightBit = arr.slice(middle,arr.length);
  if (leftBit.length > 1) {
    splitArr(leftBit, result)
  } else {
    result.push(leftBit);
  }
  if (rightBit.length > 1) {
    splitArr(rightBit, result)
  } else {
    result.push(rightBit);
  }
  return result;
}

// ----- code for testing the 2 algorithms -----

// >>>>> WRITE YOUR TESTS HERE

var n = 16 ;
var dataset = [] ;

function randomData(n) {
  var dataset = []
  for (var j=0;j<n;j++){
    dataset.push(Math.floor(Math.random() * (100-1)) +1);
  }
  return dataset;
}

console.log(randomData(16));
randomData(16)
console.log(result);

console.log(bubbleSort(dataset));

console.log(mergeSort(dataset));
// ----- p5 stuff -----

function setup() {
  createCanvas(600, 300);
}

function draw() {
  background(141, 7, 246);
}
