var featuredItems = [
  {
    id: 1,
    title: "Sporks",
    startingPrice: "£20.00",
    description: "Set the table in style",
    imageUrls: "images/sporks.jpg",
    sale: 'True',
  },
  {
    id: 2,
    title: "Milk 1pt",
    startingPrice: "£2.00",
    description: "A lovely pint of milk",
    imageUrls: "images/milk.jpg",
    sale: "False",
  },
  {
    id: 3,
    title: "Frozen Peas",
    startingPrice: "£1.00",
    description: "Simply delicious",
    imageUrls: "images/peas.jpg",
    sale: "False",
  },
]
