var allItems = [
  {
    id: 1,
    title: "Sporks",
    startingPrice: "£20.00",
    description: "Set the table in style!",
    imageUrls: "images/sporks.jpg",
    sale: "True",
  },
  {
    id: 2,
    title: "Milk 1pt",
    startingPrice: "£2.00",
    description: "A lovely pint of milk.",
    imageUrls: "images/milk.jpg",
  },
  {
    id: 3,
    title: "Frozen Peas",
    startingPrice: "£1.00",
    description: "Simply delicious.",
    imageUrls: "images/peas.jpg",
  },
  {
    id: 4,
    title: "Nke Air Force",
    startingPrice: "£80.00",
    description: "Just do it.",
    imageUrls: "images/shoe.jpeg",
  },
  {
    id: 5,
    title: "Mac N' Cheese",
    startingPrice: "£1.50",
    description: "Just like grandma's.",
    imageUrls: "images/macncheese.jpg",
  },
  {
    id: 4,
    title: "Gold Chain",
    startingPrice: "£150",
    description: "Make a statement with this luxurious accessory.",
    imageUrls: "images/chain.jpg",
  }
]
