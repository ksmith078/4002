var html = "" ;
var parentElement = document.getElementById("allListings");



console.log("JavaScripts loaded, following items loaded:");
for (i in allItems) {
  console.log(allItems[i].title);
}

// This for loop writes the html for the featured items based on the featuredData
for (var i=0; i<allItems.length; i++) {
  html += '<div class="col-md-4">\n'
       + '<div class="featuredItem">\n'
       + "<h3>" + allItems[i].title + "</h3>\n"
       + '<div class="featuredImg">\n'
       + '<img src="' + allItems[i].imageUrls + '" alt="' + allItems[i].description + '" />\n'
       + "<p>" + allItems[i].description + "</p>\n"
       + '<p class="large-txt">' + allItems[i].startingPrice + '</p>\n'
       + '<p class="add">Add to cart</p>\n'
       + '<button class="add">Add</button>\n'
       + '<p class="small-txt">Hold image to enlarge</p>\n'
       + '</div>\n'
       + '<br class="clearfix" />\n'
       + '</div>\n'
       + '</div>\n'

}

 parentElement.innerHTML +=  '<div class="row">\n'+html;
